#!/bin/bash

echo "Que voulez-vous installer ?"
echo "python - java - code - gitlab - docker"
read app

if [ $app == python ] # Installer Python
then
    sudo apt install python3 
elif [ $app == java ] # Installer Java
then
    sudo apt install default-jdk
elif [ $app == code ] # Installer Code
then
    sudo snap install code
elif [ $app == gitlab ] # Installer GitLab
then
    sudo apt install git-all
elif [ $app == docker ] # Installer Docker
then
    sudo apt install docker.io
fi