# Sae - Installer un système

## Choix système d'exploitation

Moi choix a été vite décidé, je voulais travailler dans les mêmes conditions qu'à l'IUT donc j'ai choisi d'installer xubuntu sur un ancien ordinateur.

Cet ordinateur a été mis entièrement sous Linux, car il ne me servait plus à rien.

## Comment j'ai fait

Pour cela, j'ai donc installé xubuntu qui un est un système d'exploitation libre de type Linux, et Rufus qui est un logiciel qui permet de créer des supports bootable. J'ai donc créé une clé bootable avec xubuntu d'installé dessus.  
Puis je l'ai donc mise sur mon ordi et installer xubuntu afin de remplacer Windows par xubuntu pour mettre mon ordi entièrement sous Linux (xubuntu).  
Puis pour configurer mon environnement de travail j'ai décidé de commencer par installer vscode à l'aide du terminale pour pouvoir a la suite créer mon script bash qui demande à l'utilisatuer quoi installé parmi les propostions affiché dans le terminale.

## Autre

J'ai décidé de faire un script pour configurer mon environnement de travail pour pouvoir appliqué mes compétences acquises en cours.

## Lien GitLab

https://gitlab.com/EthanMaridat/sae-installer-un-systeme